using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SummaryStats
{
    public int CorrectQuestionsBonus { get; set; }
    public int TotalQuestions { get; set; }
    public int Streak { get; set; }
    public int RemainingLivesBonus { get; set; }
    public float TimeBonusFactor { get; set; }
    public int GrossScore => RemainingLivesBonus + CorrectQuestionsBonus;
    public int TimeBonus => Mathf.RoundToInt(GrossScore * TimeBonusFactor);
    public int TotalScore => GrossScore + TimeBonus;
    public bool Win => TimeBonus > 0 && RemainingLivesBonus > 0;
}