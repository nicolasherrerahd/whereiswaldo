using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreHandler : MonoBehaviour
{
    [SerializeField] private int bonusPerLives = 25;
    [SerializeField] private int bonusPerCorrectQuestion = 100;
    public SummaryStats Stats { get; private set; } = new SummaryStats();

    public static event Action<int, SummaryStats, Transform> OnScoreUpdated = null;

    private void Start()
    {
        OnScoreUpdated?.Invoke(0, Stats, null);
    }

    public void AddLivesBonus(Transform personPos)
    {
        Stats.RemainingLivesBonus += bonusPerLives;
        OnScoreUpdated?.Invoke(bonusPerLives, Stats, personPos);
    }

    public void AddCorrectQuestionBonus(Transform questionPos)
    {
        Stats.CorrectQuestionsBonus += bonusPerCorrectQuestion;
        OnScoreUpdated?.Invoke(bonusPerCorrectQuestion, Stats, questionPos);
    }

    public void UpdateTimeBonusFactor(float punishFactor)
    {
        Stats.TimeBonusFactor = punishFactor;
        OnScoreUpdated?.Invoke(0, Stats, null);
    }
}
