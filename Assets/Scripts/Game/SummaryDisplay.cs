using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace CrossyRoad
{
    public class SummaryDisplay : MonoBehaviour
    {
        [SerializeField] private GameObject panel = null;
        [SerializeField] private Timer timer = null;
        [SerializeField] private Text statsText = null;
        [SerializeField] private VarReplacer statsReplacer = new VarReplacer("");
        [SerializeField] private Color bonusColor = Color.green;
        [SerializeField] private Text totalScoreText = null;
        [SerializeField] private VarReplacer totalScoreReplacer = new VarReplacer("");


        private void Awake()
        {
            GameController.OnGameFinished += HandleGameFinished;
        }

        private void Start()
        {
            panel.SetActive(false);
        }

        private void OnDestroy()
        {
            GameController.OnGameFinished -= HandleGameFinished;
        }

        private void HandleGameFinished(SummaryStats summary)
        {
            panel.SetActive(true);
            string remainingLives = summary.RemainingLivesBonus > 0 ? bonusColor.ToRichText($"+{summary.RemainingLivesBonus}") : "0";
            object[] stats = new object[]
            {
                summary.Streak,
                summary.TotalQuestions,
                summary.CorrectQuestionsBonus,
                remainingLives,
                TimerDisplay.GetTimeStr(timer.CurrentTime),
                summary.TimeBonus > 0 ? bonusColor.ToRichText($"+{summary.TimeBonus}") : "0"
            };
            statsText.text = statsReplacer.GetReplacedText(stats);
            totalScoreText.text = totalScoreReplacer.GetReplacedText(summary.TotalScore);

        }
    }
}
