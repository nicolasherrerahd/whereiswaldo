using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum BonusState
{
    Max,
    Decreasing,
    Empty
}

public class TimeBonusHandler : MonoBehaviour
{
    [SerializeField] private Timer timer = null;
    [SerializeField] private ScoreHandler scoreHandler;
    [SerializeField, Range(0, 1)] private float grossScoreBonusFactor = 0.25f;
    [field: SerializeField, Range(0f, 1f)] public float StartDecreaseTimeFactor { get; set; } = 0.8f;
    [field: SerializeField, Range(0f, 1f)] public float EndDecreaseTimeFactor { get; set; } = 0.4f;

    public BonusState State
    {
        get
        {
            if (timer.NormalizedTime > StartDecreaseTimeFactor) return BonusState.Max;
            if (timer.NormalizedTime < EndDecreaseTimeFactor) return BonusState.Empty;
            return BonusState.Decreasing;
        }
    }

    private void Awake()
    {
        timer.OnTimeUpdated += HandleTimeUpdated;
    }

    private void OnDestroy()
    {
        timer.OnTimeUpdated -= HandleTimeUpdated;
    }

    private void HandleTimeUpdated(int currentTime)
    {
        float currentFactor = timer.NormalizedTime - EndDecreaseTimeFactor;
        float maxFactor = StartDecreaseTimeFactor - EndDecreaseTimeFactor;
        float timeBonusFactor = Mathf.Clamp(currentFactor / maxFactor, 0, 1) * grossScoreBonusFactor;
        scoreHandler.UpdateTimeBonusFactor(timeBonusFactor);
    }
}
