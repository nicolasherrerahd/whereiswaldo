using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] private ScoreHandler scoreHandler = null;
    [SerializeField] private LivesController livesController = null;
    [SerializeField] private Timer timer = null;
    [SerializeField] private FindableMatchChecker findableChecker = null;
    [SerializeField] private MatchFeedback feedback = null;

    private bool gameFinished;
    

    public static event Action<SummaryStats> OnGameFinished = null;

    private void Awake()
    {
        FindableMatchChecker.OnFindableCheck += HandleMatchCheck;
        FindableMatchChecker.OnFinished += Finish;
        feedback.OnFeedbackDelayPassed += CheckGameFinished;
        timer.onTimeUp.AddListener(CheckGameFinished);
    }

    private void Start()
    {
        scoreHandler.Stats.TotalQuestions = findableChecker.InitialFindablesCount;
    }

    private void OnDestroy()
    {
        FindableMatchChecker.OnFindableCheck -= HandleMatchCheck;
        FindableMatchChecker.OnFinished -= Finish;
        feedback.OnFeedbackDelayPassed -= CheckGameFinished;
    }

    private void Finish()
    {
        if (gameFinished) return;
        gameFinished = true;
        timer.Stop();
        AddRemainingLivesBonus();
        feedback.DisableAll();
        OnGameFinished?.Invoke(scoreHandler.Stats);
    }

    public void CheckGameFinished()
    {
        if (livesController.LivesAmount > 0 && timer.CurrentTime > 0) return;
        Finish();
    }

    private void AddRemainingLivesBonus()
    {
        if (scoreHandler.Stats.TimeBonus <= 0) return;
        for (int i = 0; i < livesController.LivesAmount; i++)
            scoreHandler.AddLivesBonus(null);
    }

    private void HandleMatchCheck(bool isMatch)
    {
        if (!isMatch) return;
        scoreHandler.Stats.Streak = findableChecker.CorrectFindablesCount;
        scoreHandler.AddCorrectQuestionBonus(null);
    }
}
