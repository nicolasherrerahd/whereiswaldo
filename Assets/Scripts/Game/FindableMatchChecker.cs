using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindableMatchChecker : MonoBehaviour
{
    [SerializeField] private Findable[] findables = new Findable[0];
    [SerializeField] private LivesController livesController = null;

    public int InitialFindablesCount => findables.Length;
    public int CurrentFindablesCount => remainingFindables.Count;
    public int CorrectFindablesCount { get; private set; }

    private List<Findable> remainingFindables;
    private Findable currentFindable;

    public static event Action OnFinished = null;
    public static event Action<bool> OnFindableCheck = null;
    public static event Action<Findable> OnFindableUpdated = null;

    private void Awake()
    {
        Findable.OnClick += HandleClick;
    }

    private void Start()
    {
        remainingFindables = new List<Findable>(findables);
        TryUpdateFindable();
    }

    private void OnDestroy()
    {
        Findable.OnClick -= HandleClick;
    }

    public void TryUpdateFindable()
    {
        if (remainingFindables.Count > 0)
            UpdateFindable();
        else
            OnFinished?.Invoke();
    }

    private void UpdateFindable()
    {
        currentFindable = remainingFindables.GetRandomItem(true);
        OnFindableUpdated?.Invoke(currentFindable);
    }

    private void HandleClick(Findable findable)
    {
        bool isMatch = findable == currentFindable;
        if (!isMatch)
            livesController.RemoveLives(1);
        else
            CorrectFindablesCount++;
        OnFindableCheck?.Invoke(isMatch);
    }
}
