using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinLoseDisplay : MonoBehaviour
{
    [Header("Block color")]
    [SerializeField] private Image blockImg = null;
    [SerializeField] private Sprite winSpr = null;
    [SerializeField] private Sprite loseSpr = null;
    [Header("Text value")]
    [SerializeField] private Text winLoseText = null;
    [SerializeField] private string winStr = string.Empty;
    [SerializeField] private string loseStr = string.Empty;

    private void OnEnable()
    {
        GameController.OnGameFinished += HandleGameFinished;
    }

    private void OnDisable()
    {
        GameController.OnGameFinished -= HandleGameFinished;
    }

    private void HandleGameFinished(SummaryStats stats)
    {
        blockImg.sprite = stats.Win ? winSpr : loseSpr;
        winLoseText.text = stats.Win ? winStr : loseStr;
    }
}
