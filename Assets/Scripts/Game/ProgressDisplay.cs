using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressDisplay : MonoBehaviour
{
    [SerializeField] private FindableMatchChecker checker = null;
    [SerializeField] private Image fillArea = null;
    [SerializeField] private Text progressText = null;
    [SerializeField] private VarReplacer progressReplacer = new VarReplacer("{c}/{t}");

    private void Awake()
    {
        FindableMatchChecker.OnFindableUpdated += HandleFindableUpdated;
    }

    private void OnDestroy()
    {
        FindableMatchChecker.OnFindableUpdated -= HandleFindableUpdated;
    }

    private void HandleFindableUpdated(Findable findable)
    {
        float doneCount = checker.InitialFindablesCount - checker.CurrentFindablesCount; 
        fillArea.fillAmount = doneCount / checker.InitialFindablesCount;
        progressText.text = progressReplacer.GetReplacedText(new object[] { doneCount, checker.InitialFindablesCount });
    }
}
