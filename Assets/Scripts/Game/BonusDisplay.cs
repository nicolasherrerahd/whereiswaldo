using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonusDisplay : MonoBehaviour
{
    [SerializeField] private Text text = null;
    [SerializeField] private VarReplacer replacer = new VarReplacer("{b}");

    private void Awake()
    {
        ScoreHandler.OnScoreUpdated += HandleScoreUpdated;
    }

    private void OnDestroy()
    {
        ScoreHandler.OnScoreUpdated -= HandleScoreUpdated;
    }

    private void HandleScoreUpdated(int bonus, SummaryStats stats, Transform transform)
    {
        var scoreValues = new object[]
        {
                stats.TimeBonus > 0 ? $" +{stats.TimeBonus}" : "0"
        };
        text.text = replacer.GetReplacedText(scoreValues);
    }
}
