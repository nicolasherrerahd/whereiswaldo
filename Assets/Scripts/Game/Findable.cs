using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Findable : MonoBehaviour
{
    [SerializeField] private SpriteRenderer sprRenderer = null;
    [SerializeField] private float scaleFactor = 1.2f;
    [SerializeField] private int maxLayer = 99;
    [field: SerializeField, TextArea(2, 5)] public string Question { get; private set; }
    [field: SerializeField] public Sprite Tip { get; private set; }

    public static event Action<Findable> OnClick = null;

    private Vector3 originalScale;
    private int originalLayer;

    private void Start()
    {
        originalScale = transform.parent.localScale;
        originalLayer = sprRenderer.sortingOrder;
    }

    private void OnMouseOver()
    {
        transform.parent.localScale = originalScale * scaleFactor;
        sprRenderer.sortingOrder = maxLayer;
    }

    private void OnMouseExit()
    {
        transform.parent.localScale = originalScale;
        sprRenderer.sortingOrder = originalLayer;
    }

    private void OnMouseDown()
    {
        OnClick?.Invoke(this);
    }

}
