using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionDisplay : MonoBehaviour
{
    [SerializeField] private Text text = null;
    private void Awake()
    {
        FindableMatchChecker.OnFindableUpdated += HandleFindableUpdated;
    }

    private void OnDestroy()
    {
        FindableMatchChecker.OnFindableUpdated -= HandleFindableUpdated;
    }

    private void HandleFindableUpdated(Findable findable)
    {
        text.text = findable.Question;
    }
}
