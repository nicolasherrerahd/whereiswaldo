using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MatchFeedback : MonoBehaviour
{
    [SerializeField] private GameObject reviewPanel = null;
    [SerializeField] private GameObject findableBlocker = null;
    [SerializeField] private GameObject correctPanel = null;
    [SerializeField] private GameObject wrongPanel = null;
    [SerializeField] private GameObject tipPanel = null;
    [SerializeField] private Image tip = null;
    [SerializeField] private float feedbackDelay = 3f;
    [SerializeField] private Timer timer = null;

    public event Action OnFeedbackDelayPassed = null;

    private void Awake()
    {
        FindableMatchChecker.OnFindableCheck += HandleCheck;
        FindableMatchChecker.OnFindableUpdated += HandleFindableUpdated;
    }

    private void Start()
    {
        DisableAll();
    }

    public void DisableAll()
    {
        reviewPanel.SetActive(false);
        findableBlocker.SetActive(false);
        tipPanel.SetActive(false);
    }

    private void OnDestroy()
    {
        FindableMatchChecker.OnFindableCheck -= HandleCheck;
        FindableMatchChecker.OnFindableUpdated -= HandleFindableUpdated;
    }

    private void HandleCheck(bool isMatch)
    {
        StartCoroutine(CallPanelsEnabling(isMatch));
    }

    private IEnumerator CallPanelsEnabling(bool isMatch)
    {
        reviewPanel.SetActive(true);
        findableBlocker.SetActive(true);
        correctPanel.SetActive(isMatch);
        wrongPanel.SetActive(!isMatch);
        timer.PauseTimer(true);
        yield return new WaitForSeconds(feedbackDelay);
        reviewPanel.SetActive(false);
        if (!isMatch)
        {
            findableBlocker.SetActive(false);
            timer.PauseTimer(false);
        }
        else
            tipPanel.SetActive(true);
        OnFeedbackDelayPassed?.Invoke();
    }

    private void HandleFindableUpdated(Findable findable)
    {
        tip.sprite = findable.Tip;
        findableBlocker.SetActive(false);
        timer.PauseTimer(false);
        tipPanel.SetActive(false);
    }
}
