using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupNameSorter : MonoBehaviour
{
    [SerializeField] private string childName = "GameObject";
    [SerializeField] private bool refresh = false;

    private void OnValidate()
    {
        if (!refresh) return;
        refresh = false;
        foreach (Transform child in transform)
            child.name = $"{childName} ({child.GetSiblingIndex()})";
    }
}
